Name:           python-pytest-expect
Version:        1.1.0
Release:        5
Summary:        Py.test plugin to store test expectations and mark tests based on them
License:        MIT
URL:            https://pypi.org/project/pytest-expect/
Source0:        https://files.pythonhosted.org/packages/8b/3d/c5fcbb8a693dcde00ecc3d69639b2b2b3f385b305bc76a06f94f1030b2dc/pytest-expect-1.1.0.tar.gz
Source1:        https://raw.githubusercontent.com/gsnedders/pytest-expect/%{version}/LICENSE

BuildRequires:  python3-devel python3-setuptools
BuildArch:      noarch

%description
A py.test plugin that stores test expectations by saving the set of failing
tests, allowing them to be marked as xfail when running them in future.
The tests expectations are stored such that they can be distributed alongside
the tests.


%package -n     python3-pytest-expect
Summary:        Python3 package
%{?python_provide:%python_provide python3-pytest-expect}

%description -n python3-pytest-expect
A py.test plugin that stores test expectations by saving the set of failing
tests, allowing them to be marked as xfail when running them in future.
The tests expectations are stored such that they can be distributed alongside
the tests.

%prep
%autosetup -n pytest-expect-1.1.0 -p1
cp -p %{SOURCE1} .


%build
%py3_build

%install
%py3_install


%files -n python3-pytest-expect
%doc README.rst LICENSE
%{python3_sitelib}/pytest_expect
%{python3_sitelib}/pytest_expect-%{version}-py?.??.egg-info

%changelog
* Wed Dec 22 2021 caodongxia <caodongxia@huawei.com> 1.1.0-5
- Adapt to python3.10

* Mon Sep 27 2021 lingsheng <lingsheng@huawei.com> 1.1.0-4
- Provide python-pytest-expect

* Tue Feb 25 2020 chenli<chenli147@huawei.com> 1.1.0-3
- Modify Spec

* Mon Jan 6 2020 chenli<chenli147@huawei.com> 1.1.0-2
- Initial package
